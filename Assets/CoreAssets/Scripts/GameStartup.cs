﻿using UnityEngine;
using System.Collections;

public class GameStartup : MonoBehaviour
{
	[SerializeField]
	private int TargetFramerate;

	// Use this for initialization
	void Awake ()
	{
		Application.targetFrameRate = 60;
	}
}
