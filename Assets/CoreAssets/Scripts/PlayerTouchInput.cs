﻿using UnityEngine;
using System.Collections;

public class PlayerTouchInput : MonoBehaviour
{
	private Vector3 targetDestination;
	private ShipSteering shipSteering;
	private GameObject shipObject;
	public float targetDestinationDeadzoneRadius;
	bool isSteeringRight = false;
	bool isSteeringLeft = false;
	bool isAccelerating = false;
	bool isDecelerating = false;

	void Start()
	{
		shipSteering = GetComponent<ShipSteering>();
		shipObject = gameObject;
		targetDestination = gameObject.transform.position;
		GetComponent<ShipShoot>().BeginShootingContinuously();
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.touchCount > 0 && 
			Input.GetTouch (0).phase == TouchPhase.Ended) 
		{
			Vector2 scrPoint = Input.GetTouch (0).position;
			Ray touchRay = Camera.main.ScreenPointToRay (new Vector3 (scrPoint.x, scrPoint.y));

			RaycastHit rayHit;
			if (Physics.Raycast (touchRay, out rayHit)) 
			{

				targetDestination = rayHit.point;
			}
		}

		// Generate direction towards the destination.
		Vector3 vectorToDestination = targetDestination - shipObject.transform.position;

		if (vectorToDestination.magnitude <= targetDestinationDeadzoneRadius) 
		{
			targetDestination = shipObject.transform.position;

			if (isSteeringLeft) 
			{
				shipSteering.EndSteerLeft ();
				isSteeringLeft = false;
			}

			if (isSteeringRight) 
			{
				shipSteering.EndSteerRight ();
				isSteeringRight = false;
			}

			if (isAccelerating) 
			{
				shipSteering.EndAcceleration ();
				isAccelerating = false;
			}

			if (isDecelerating) 
			{
				shipSteering.EndDeceleration ();
				isDecelerating = false;
			}

			return;
		} 
		else 
		{
			Vector3 vectorToDestinationNormalized = vectorToDestination.normalized;

			if (vectorToDestinationNormalized.x > 0) 
			{
				if( isSteeringLeft )
				{
					isSteeringLeft = false;
					shipSteering.EndSteerLeft();
				}

				shipSteering.BeginSteerRight();
				isSteeringRight = true;
			} 
			else if (vectorToDestinationNormalized.x < 0) 
			{
				if( isSteeringRight )
				{
					isSteeringRight = false;
					shipSteering.EndSteerRight();
				}

				shipSteering.BeginSteerLeft();
				isSteeringLeft = true;
			}

			if (vectorToDestinationNormalized.y > 0) 
			{
				if( isDecelerating )
				{
					isDecelerating = false;
					shipSteering.EndDeceleration();
				}
				
				shipSteering.BeginAcceleration();
				isAccelerating = true;
			}
			else if (vectorToDestinationNormalized.y < 0) 
			{
				if( isAccelerating )
				{
					isAccelerating = false;
					shipSteering.EndAcceleration();
				}
				
				shipSteering.BeginDeceleration();
				isDecelerating = true;
			} 
		}
	}
}
