﻿using UnityEngine;
using System.Collections;

public class AsteroidSpawn : MonoBehaviour 
{
	private IEnumerator spawnCoroutine;

	[SerializeField]
	private GameObject asteroidPrefab;

	[SerializeField]
	private AudioSource soundCollideWithAsteroid;

	[SerializeField]
	private AudioSource soundCollideWithBullet;

	[SerializeField]
	private AudioSource soundDestruction;

	[SerializeField]
	private GameObject particleCollideWithAsteroidPrefab;

	public Rect spawnRect;

	[Range(0.001f,1f)]
	public float asteroidMinScale;

	[Range(0.001f,1f)]
	public float asteroidMaxScale;

	public float timeBetweenSpawn;

	// Use this for initialization
	void Start () 
	{
		spawnCoroutine = SpawnContinuously();
		BeginSpawning();
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;

		Gizmos.DrawLine(new Vector3( spawnRect.xMin,spawnRect.yMax),
		               new Vector3( spawnRect.xMax,spawnRect.yMax));

		Gizmos.DrawLine(new Vector3( spawnRect.xMin,spawnRect.yMin),
		               new Vector3( spawnRect.xMax,spawnRect.yMin));

		Gizmos.DrawLine(new Vector3( spawnRect.xMin,spawnRect.yMax),
		               new Vector3( spawnRect.xMin,spawnRect.yMin));
	
		Gizmos.DrawLine(new Vector3( spawnRect.xMax,spawnRect.yMax),
		               new Vector3( spawnRect.xMax,spawnRect.yMin));
	}

	public void BeginSpawning()
	{
		StartCoroutine( spawnCoroutine );
	}

	public void EndSpawning()
	{
		StopCoroutine( spawnCoroutine );
	}

	private IEnumerator SpawnContinuously()
	{
		while(true)
		{
			Vector3 spawnPosition = new Vector3(
				Random.Range(spawnRect.xMin,spawnRect.xMax),
				Random.Range(spawnRect.yMin,spawnRect.yMax),
				0 );

			Quaternion spawnRotation = Quaternion.Euler(
				Random.Range(0,359),
				Random.Range(0,359),
				Random.Range(0,359) );

			float asteroidScale = Random.Range(asteroidMinScale,asteroidMaxScale);

			GameObject asteroid = Instantiate( asteroidPrefab, spawnPosition, spawnRotation) as GameObject;
			asteroid.GetComponent<AsteroidCollision>().Initialize(asteroidScale,
			                                                      soundCollideWithAsteroid,
			                                                      soundCollideWithBullet,
			                                                      soundDestruction,
			                                                      particleCollideWithAsteroidPrefab );

			yield return new WaitForSeconds(timeBetweenSpawn);
		}
	}
}
