﻿using UnityEngine;
using System.Collections;

public class ProjectileCollision : MonoBehaviour
{
	[SerializeField]
	GameObject collideParticlePrefab;

	void OnCollisionEnter(Collision collision) 
	{
		if( collision.gameObject.CompareTag( "Asteroid"))
		{
			Destroy( gameObject );

			Instantiate(collideParticlePrefab,
			            gameObject.transform.position,
			            Quaternion.identity );
		}
	}
}
