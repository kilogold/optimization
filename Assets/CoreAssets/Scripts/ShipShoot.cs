﻿using UnityEngine;
using System.Collections;

public class ShipShoot : MonoBehaviour 
{
	[SerializeField]
	private AudioSource sfxShoot;

	[SerializeField]
	private GameObject projectilePrefab;
	private IEnumerator shootingCoroutine;

	public Transform[] projectileSpawnPoints;
	public float delayBetweenShots;

	public bool IsShooting { private set; get;}

	public enum FireMode { SameTime, SplitTime};
	public FireMode fireMode;
	
	private int lastFiredCannon;

	void Awake()
	{
		shootingCoroutine = ShootContinuously();
		fireMode = FireMode.SplitTime;
		lastFiredCannon = 0;
	}

	private void FireProjectile()
	{
		switch (fireMode) 
		{
			case FireMode.SameTime:
				for (int curSpawnPointIndex = 0; curSpawnPointIndex < projectileSpawnPoints.Length; curSpawnPointIndex++) 
				{
					SpawnProjectile(curSpawnPointIndex);
				}
				break;

			case FireMode.SplitTime:
				if( lastFiredCannon >= projectileSpawnPoints.Length )
				{
					lastFiredCannon = 0;
				}
				
				SpawnProjectile(lastFiredCannon);
				
				lastFiredCannon++;
				break;

			default:
				break;
		}
	}

	private void SpawnProjectile( int spawnIndex )
	{
		GameObject.Instantiate(projectilePrefab,projectileSpawnPoints[spawnIndex].position, Quaternion.identity);
		projectileSpawnPoints[spawnIndex].GetComponentInChildren<ParticleSystem>().Play();
		sfxShoot.Play();
	}

	public void BeginShootingContinuously()
	{
		StartCoroutine( shootingCoroutine );
	}

	public void EndShootingContinuously()
	{
		StopCoroutine( shootingCoroutine );
	}

	private IEnumerator ShootContinuously()
	{
		while(true)
		{
			FireProjectile();

			yield return new WaitForSeconds(delayBetweenShots);
		}
	}
}
