﻿using UnityEngine;
using System.Collections;

public class AsteroidHealth : MonoBehaviour 
{
	[SerializeField]
	private int currentHealth;

	public int CurrentHealth { get { return currentHealth;} }

	[SerializeField]
	private int maxHealth = 100;

	// Use this for initialization
	void Start () 
	{
		currentHealth = maxHealth;
	}

	public void TakeDamage( int damage )
	{
		currentHealth -= damage;

		if( currentHealth <= 0 )
		{
			Destroy(gameObject);
		}
	}
}
