﻿using UnityEngine;
using System.Collections;

public class AsteroidCollision : MonoBehaviour 
{
	private AudioSource soundCollideWithAsteroid;
	private AudioSource soundCollideWithBullet;
	private AudioSource soundDestruction;
	private GameObject particleCollideWithAsteroidPrefab;

	private AsteroidHealth asteroidHealth;


	public void Initialize( float asteroidScale,
	                        AudioSource soundCollideWithAsteroidIn,
	                        AudioSource soundCollideWithBulletIn,
	                        AudioSource soundDestructionIn,
	                       	GameObject particleCollideWithAsteroidPrefabIn )
	{
		GetComponent<Rigidbody>().mass *= asteroidScale;
		transform.localScale *= asteroidScale;

		soundCollideWithAsteroid = soundCollideWithAsteroidIn;
		soundCollideWithBullet = soundCollideWithBulletIn;
		soundDestruction = soundDestructionIn;
		particleCollideWithAsteroidPrefab = particleCollideWithAsteroidPrefabIn;
	}

	void Start()
	{
		asteroidHealth = GetComponent<AsteroidHealth>();
	}

	void OnCollisionEnter(Collision collision) 
	{
		if( collision.gameObject.CompareTag("PlayerProjectile") )
		{
			ProjectileStats projectileStats = collision.gameObject.GetComponent<ProjectileStats>();

			asteroidHealth.TakeDamage( projectileStats.projectileDamage );
		}

		if ( collision.gameObject.CompareTag( "LevelBounds" ) )
		{
			Destroy( gameObject );
		}

		if( collision.gameObject.CompareTag( "Asteroid" ) )
		{
			Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
			Renderer renderer = GetComponent<Renderer>();

			// Is the asteroid within the bounds?
			if(GeometryUtility.TestPlanesAABB(planes, renderer.bounds))
			{
				soundCollideWithAsteroid.Play();

				Quaternion spawnRotation = Quaternion.Euler(
					Random.Range(0,359),
					0,
					Random.Range(0,359) );

				Vector3 spawnPosition = new Vector3(collision.contacts[0].point.x,
				                                    collision.contacts[0].point.y,
				                                    -10.0f );

				GameObject particles = Instantiate(	particleCollideWithAsteroidPrefab,
										            spawnPosition,
										            spawnRotation) as GameObject;

				particles.transform.parent = gameObject.transform;
			} 
		}
	}
}
