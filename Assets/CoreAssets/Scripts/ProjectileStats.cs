﻿using UnityEngine;
using System.Collections;

public class ProjectileStats : MonoBehaviour 
{
	public int projectileDamage;

	[SerializeField]
	private float projectileLifetime;

	void Start()
	{
		Destroy(gameObject,projectileLifetime);
	}
}
