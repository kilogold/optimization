﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour 
{
	private ShipSteering shipSteering;
	private ShipShoot shipShoot;

	void Start()
	{
		shipSteering = GetComponent<ShipSteering>();
		shipShoot = GetComponent<ShipShoot>();
	}

	// Update is called once per frame
	void Update () 
	{
		if( Input.GetKey(KeyCode.A) )
		{
			shipSteering.BeginSteerLeft();
		}

		if( Input.GetKeyUp(KeyCode.A))
		{
			shipSteering.EndSteerLeft();
		}

		if( Input.GetKey(KeyCode.D) )
		{
			shipSteering.BeginSteerRight();
		}
		
		if( Input.GetKeyUp(KeyCode.D))
		{
			shipSteering.EndSteerRight();
		}

		if( Input.GetKey(KeyCode.S) )
		{
			shipSteering.BeginDeceleration();
		}
		
		if( Input.GetKeyUp(KeyCode.S))
		{
			shipSteering.EndDeceleration();
		}

		if( Input.GetKey(KeyCode.W) )
		{
			shipSteering.BeginAcceleration();
		}
		
		if( Input.GetKeyUp(KeyCode.W))
		{
			shipSteering.EndAcceleration();
		}

		if( Input.GetKeyDown(KeyCode.Space)  )
		{
			// Other forms of input may have already activated 
			// continuous shooting.
			if( false == shipShoot.IsShooting )
			{
				shipShoot.BeginShootingContinuously();
			}
		}

		if( Input.GetKeyUp(KeyCode.Space)  )
		{
			shipShoot.EndShootingContinuously();
		}
	}
}
