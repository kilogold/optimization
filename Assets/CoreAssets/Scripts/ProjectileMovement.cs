﻿using UnityEngine;
using System.Collections;

public class ProjectileMovement : MonoBehaviour 
{
	public float projectileForceImpulse;

	// Update is called once per frame
	void Start () 
	{
		gameObject.GetComponent<Rigidbody>().AddForce(0,projectileForceImpulse,0,ForceMode.VelocityChange);
	}
}
