﻿using UnityEngine;
using System.Collections;

public class ShipCollision : MonoBehaviour
{
	[SerializeField]
	private AudioSource sfxShipCollide;

	void OnCollisionEnter (Collision collision)
	{
		if (collision.gameObject.CompareTag ("Asteroid")) 
		{
			LightFlicker[] lightFlickerArray = gameObject.GetComponentsInChildren<LightFlicker> ();
		
			foreach (LightFlicker curFlicker in lightFlickerArray) 
			{
				if (curFlicker.gameObject.name == "HullLight") 
				{
					curFlicker.FlickerForSeconds (1.0f);
					break;
				}
			}

			sfxShipCollide.Play();
		}
	}
}
