﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipSteering : MonoBehaviour 
{
	public float steeringForce;
	public float accelForce;
	private float playerShipThrusterMagnitude;
	private GameObject playerShip;
	private List<ParticleSystem> playerShipThrustersList;
	private float thrusterOriginalSize;

	void Start()
	{
		playerShip = gameObject;

		GameObject[] thrusters = GameObject.FindGameObjectsWithTag("ShipThruster");
		playerShipThrustersList = new List<ParticleSystem>(thrusters.Length);

		foreach (GameObject curThruster in thrusters) 
		{
			playerShipThrustersList.Add(curThruster.GetComponent<ParticleSystem>());
		}

		thrusterOriginalSize = playerShipThrustersList[0].startSize;
	}

	public void BeginSteerRight()
	{
		playerShip.transform.rotation = Quaternion.Euler(0,-45,0);
		
		playerShip.GetComponent<Rigidbody>().AddForce(steeringForce,0,0);
	}

	public void EndSteerRight()
	{
		playerShip.transform.rotation = Quaternion.Euler(0,0,0);
	}

	public void BeginSteerLeft()
	{
		playerShip.transform.rotation = Quaternion.Euler(0,45,0);

		playerShip.GetComponent<Rigidbody>().AddForce(-steeringForce,0,0);
	}

	public void EndSteerLeft()
	{
		playerShip.transform.rotation = Quaternion.Euler(0,0,0);
	}

	public void BeginAcceleration()
	{
		if( Mathf.Approximately(thrusterOriginalSize, playerShipThrustersList[0].startSize ) )
		{
			foreach (ParticleSystem curParticleSys in playerShipThrustersList) 
			{
				curParticleSys.startSize *= 2;
			}
		}

		playerShip.GetComponent<Rigidbody>().AddForce(0,accelForce,0);
	}

	public void EndAcceleration()
	{
		foreach (ParticleSystem curParticleSys in playerShipThrustersList) 
		{
			curParticleSys.startSize /= 2;
		}
	}

	public void BeginDeceleration()
	{
		if( Mathf.Approximately(thrusterOriginalSize, playerShipThrustersList[0].startSize ) )
		{
			foreach (ParticleSystem curParticleSys in playerShipThrustersList) 
			{
				curParticleSys.startSize /= 2;
			}
		}

		playerShip.GetComponent<Rigidbody>().AddForce(0,-accelForce,0);

	}

	public void EndDeceleration()
	{
		foreach (ParticleSystem curParticleSys in playerShipThrustersList) 
		{
			curParticleSys.startSize *= 2;
		}
	}
}
