﻿using UnityEngine;
using System.Collections;

public class LightFlicker : MonoBehaviour 
{
	private Light lightSource;
	private float originalIntensity;
	public float dimSpeed;

	[SerializeField, Range(0,8.0f)]
	private float minIntensity;

	[SerializeField, Range(0,8.0f)]
	private float maxIntensity;

	private IEnumerator flickerCoroutine;

	public bool IsFlickering 
	{ 
		set { this.enabled = value; } 
		get { return this.enabled; } 
	}

	void OnValidate()
	{
		if(minIntensity > maxIntensity || maxIntensity < minIntensity )
		{
			maxIntensity = minIntensity;
		}
	}

	// Use this for initialization
	void Awake () 
	{
		lightSource = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		lightSource.intensity += Time.deltaTime * dimSpeed;
		if (lightSource.intensity >= maxIntensity) 
		{
			lightSource.intensity = minIntensity;
		}
	}

	void OnEnable()
	{
		originalIntensity = lightSource.intensity;
	}

	void OnDisable()
	{
		lightSource.intensity = originalIntensity;
	}

	public void FlickerForSeconds( float seconds )
	{
		if( true == IsFlickering )
		{
			StopCoroutine( flickerCoroutine );
		}

		flickerCoroutine = TimedFlicker(seconds);
		StartCoroutine( flickerCoroutine );
	}

	private IEnumerator TimedFlicker( float seconds )
	{
		IsFlickering = true;

		yield return new WaitForSeconds(seconds);

		IsFlickering = false;
	}
}
